'use strict'
const Database = use('Database')
const Config = use('Config')
const graylog2 = use("graylog2");

class LogService {

    async addGraylog () {

        var data = new Array();

        data['end_point'] = '/test2'
        data['execute_time'] = '0ms'
        data['level'] = '5'
        data['relation_id'] = 'id'
        data['request'] = 'params:{}'
        data['response'] = 'params:{}'
        data['source'] = '/'

        const result = ''
        const logger = new graylog2.graylog({
            
            servers: [
                { 'host': 'elog.eggdigital.com', port: 12201 }
            ],
            hostname: 'elog.eggdigital.com',            
            facility: 'localhost',             
            bufferSize: 1350,        
        });

        logger.log("Test add graylog", {
            endpoint: data['end_point'],
            execute_time: data['execute_time'],
            level: data['level'],
            relation_id: data['relation_id'],
            request: data['request'],
            response: data['response'],
            source: data['source']
        });
        logger.on("error", function (error) {
            console.error("Error while trying to write to graylog2:", error);
        });

        logger.close(function(){
            console.log('All done');
            process.exit();
        });

        return result
    }
}

module.exports = LogService
