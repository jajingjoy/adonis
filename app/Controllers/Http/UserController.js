'use strict'
const Database = use('Database')
const Config = use('Config')
const { validate } = use('Validator')
const LogService  = use('App/Services/LogService')

class UserController extends LogService {

    // async login ({ request, auth }) {
    //     const { email, password } = request.all()
    //     await auth.attempt(email, password)

    //     return 'Logged in successfully'
    // }

    // show ({ auth, params }) {
    // if (auth.user.id !== Number(params.id)) {
    //   return 'You cannot see someone else\'s profile'
    // }
    // return auth.user
    // }

    async index (request, response) {
        // var start = now()
        console.time('getUser');
        var page = request.request.input('page', 1)
        var limit = request.request.input('limit', 50)
        var q = request.request.input('q')
        // const rules = {
        //     page: 'required|integer',
        //     limit: 'required|integer'
        // }
      
        //const validation = await validate(request.request.get(), rules)
      
        // if (validation.fails()) {
        //     return validation.messages();
        // }
        var result = await Database
            .select(['id','username','password'])
            .table('users')
            .paginate(page, limit)

        // var end = now()
        console.timeEnd('getUser');
        
        this.addGraylog()
    
        return result
    }
}

module.exports = UserController
